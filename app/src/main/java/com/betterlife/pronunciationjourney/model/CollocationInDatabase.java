package com.betterlife.pronunciationjourney.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by nguyen on 14/03/2018.
 */

public class CollocationInDatabase extends RealmObject{
    @PrimaryKey
    @Required
    private String sentences;

    private String meaning;
    private String urlAudio;

    public CollocationInDatabase() {
    }

    public CollocationInDatabase(String sentences, String meaning, String urlAudio) {
        this.sentences = sentences;
        this.meaning = meaning;
        this.urlAudio = urlAudio;
    }

    public String getSentences() {
        return sentences;
    }

    public void setSentences(String sentences) {
        this.sentences = sentences;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getUrlAudio() {
        return urlAudio;
    }

    public void setUrlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
    }
}
