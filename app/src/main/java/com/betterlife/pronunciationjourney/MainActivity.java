package com.betterlife.pronunciationjourney;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.betterlife.pronunciationjourney.fragment.FragmentChallenge;
import com.betterlife.pronunciationjourney.fragment.FragmentHome;
import com.betterlife.pronunciationjourney.fragment.FragmentListPopularCollocation;
import com.betterlife.pronunciationjourney.fragment.FragmentListPopularWord;
import com.betterlife.pronunciationjourney.fragment.FragmentPractice;
import com.betterlife.pronunciationjourney.utils.OnClickDialogInterface;
import com.betterlife.pronunciationjourney.utils.ScreenManager;

public class MainActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpView();
        openFragmentHome();

    }

    private void openFragmentHome() {
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.fragment_container,new FragmentHome(),false,FragmentHome.TAG);
    }

    private void setUpView() {
        mToolbar = findViewById(R.id.toolbar);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                item.setChecked(true);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                switch (item.getItemId()){
                    case R.id.nav_popular_word:
                        openFragmentWordIfNecessary();
                        break;
                    case R.id.nav_popular_sentences:
                        openFragmentSentenceIfNecessary();
                        break;
                    case R.id.nav_practice:
                        openFragmentPractice();
                        break;
                    case R.id.nav_challenge:
                        openFragmentChallenge();
                        break;
                    case R.id.nav_another_app:
                        Toast.makeText(MainActivity.this,"See another app",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_rate_five_star:
                        rateApp();
                        break;
                    case R.id.nav_setting:
                        Toast.makeText(MainActivity.this,"Choose setting",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.nav_hide_ads:
                        Toast.makeText(MainActivity.this,"Hide ads",Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });
    }

    private void rateApp() {
        try {
            Intent rateIntent = new Intent(Intent.ACTION_VIEW);
            rateIntent.setData(Uri.parse(getString(R.string.app_market_id)
                    + getPackageName()));
            startActivity(rateIntent);
        } catch (Exception e) {
            Log.e("RateApp",e.toString()+"");
        }
    }

    private void openFragmentChallenge() {
        popBack2FragmentHome();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.fragment_container,new FragmentChallenge(),
                true,FragmentPractice.TAG);
    }

    private void openFragmentPractice() {
        popBack2FragmentHome();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.fragment_container,new FragmentPractice(),
                true,FragmentPractice.TAG);
    }

    private void openFragmentSentenceIfNecessary() {
        popBack2FragmentHome();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.fragment_container,new FragmentListPopularCollocation(),
                true,FragmentListPopularCollocation.TAG);
    }

    private void openFragmentWordIfNecessary() {
        popBack2FragmentHome();
        ScreenManager.getInst().openFragmentWithAnimation(getSupportFragmentManager(),R.id.fragment_container,new FragmentListPopularWord(),
                true,FragmentListPopularWord.TAG);

    }

    private void popBack2FragmentHome(){
        int numberFragmentInBackStack = getSupportFragmentManager().getBackStackEntryCount();
        for(int i=0;i<numberFragmentInBackStack-1;i++){
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        final FragmentPractice fragmentPractice = (FragmentPractice) getSupportFragmentManager().findFragmentByTag(FragmentPractice.TAG);
        FragmentHome fragmentHome = (FragmentHome) getSupportFragmentManager().findFragmentByTag(FragmentHome.TAG);
        FragmentChallenge fragmentChallenge = (FragmentChallenge) getSupportFragmentManager().findFragmentByTag(FragmentChallenge.TAG);
        if(fragmentPractice != null){
            if(fragmentPractice.isVisible()){
                ScreenManager.getInst().showDialog(MainActivity.this, "", getString(R.string.want_exit_app),
                        new OnClickDialogInterface() {
                            @Override
                            public void onClickOK() {
                                fragmentPractice.saveScore();
                                MainActivity.super.onBackPressed();
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                return;
            }
        }
        if(fragmentHome!=null){
            if(fragmentHome.isVisible()){
                ScreenManager.getInst().showDialog(MainActivity.this, "", getString(R.string.want_exit_practice),
                        new OnClickDialogInterface() {
                            @Override
                            public void onClickOK() {
                                MainActivity.super.onBackPressed();
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                return;
            }
        }
        if(fragmentChallenge != null){
            if(fragmentChallenge.isVisible()){
                ScreenManager.getInst().showDialog(MainActivity.this, "", getString(R.string.want_exit_challenge),
                        new OnClickDialogInterface() {
                            @Override
                            public void onClickOK() {
                                MainActivity.super.onBackPressed();
                            }

                            @Override
                            public void onClickCancel() {

                            }
                        });
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
