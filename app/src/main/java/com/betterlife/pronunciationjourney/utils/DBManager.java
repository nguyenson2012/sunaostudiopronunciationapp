package com.betterlife.pronunciationjourney.utils;

import com.betterlife.pronunciationjourney.model.CollocationInDatabase;
import com.betterlife.pronunciationjourney.model.WordObject;

import java.util.List;

import io.realm.Realm;

/**
 * Created by nguyen on 14/03/2018.
 */

public class DBManager {
    private static DBManager instance;
    private Realm realm;

    private DBManager() {
        realm=Realm.getDefaultInstance();
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public void savePopularWord(WordObject wordObject){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(wordObject);
        realm.commitTransaction();
    }

    public List<WordObject> getAllWordPopular(){
        return realm.where(WordObject.class).findAll();
    }

    public void saveCollocation(CollocationInDatabase collocationInDatabase){
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(collocationInDatabase);
        realm.commitTransaction();
    }

    public List<CollocationInDatabase> getAllCollocation(){
        return realm.where(CollocationInDatabase.class).findAll();
    }
}
