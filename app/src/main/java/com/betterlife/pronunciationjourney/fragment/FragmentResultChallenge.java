package com.betterlife.pronunciationjourney.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.betterlife.pronunciationjourney.R;

/**
 * Created by nguyen on 13/03/2018.
 */

public class FragmentResultChallenge extends Fragment {
    public static final String TAG = "FragmentResultChallenge";
    private TextView mTextViewTotalTime;
    private Button mButtonShareResult;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result_challenge,container,false);
        setupView(rootView);
        return rootView;
    }

    private void setupView(View rootView) {
        mTextViewTotalTime = rootView.findViewById(R.id.result_textview_total_time);
        mButtonShareResult = rootView.findViewById(R.id.result_button_share_result);

    }
}
